package org.libindic.libindik.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LibindikApplication

fun main(args: Array<String>) {
    runApplication<LibindikApplication>(*args)
}
