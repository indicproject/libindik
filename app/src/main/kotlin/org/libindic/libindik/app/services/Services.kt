package org.libindic.libindik.app.services

import org.libindic.libindik.libs.Normaliser
import org.libindic.libindik.libs.Payyans
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class NormaliserService {
    val normaliser = Normaliser()
    fun normaliseText(input: String): String =
        normaliser.normaliseText(input)
}

@Service
class PayyansService(
    @Autowired normaliserService: NormaliserService
) {
    val payyans = Payyans(normaliser = normaliserService.normaliser)

    fun ascii2Unicode(input: String, fontName: String): String = payyans.ascii2Unicode(input, fontName)
    fun unicode2Ascii(input: String, fontName: String): String = payyans.unicode2Ascii(input, fontName)

    fun ascii2UnicodeWithNormalisation(input: String, fontName: String): String =
        payyans.ascii2UnicodeWithNormalisation(input, fontName)
    fun unicode2AsciiWithNormalisation(input: String, fontName: String): String =
        payyans.unicode2AsciiWithNormalisation(input, fontName)
}
