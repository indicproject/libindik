package org.libindic.libindik.app.api

import org.libindic.libindik.app.services.NormaliserService
import org.libindic.libindik.app.services.PayyansService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PathVariable

@RestController
class Api(
    @Autowired val normaliserService: NormaliserService,
    @Autowired val payyansService: PayyansService,
) {
    @PostMapping("/normaliser")
    fun normaliseString(
        @RequestBody input: String,
    ) = normaliserService.normaliseText(input)

    @PostMapping("/payyans/ascii2unicode/{fontName}")
    fun asciiToUnicode(
        @RequestBody input: String,
        @PathVariable fontName: String,
        @RequestParam normalise: Boolean
    ) =
        if(normalise) payyansService.ascii2UnicodeWithNormalisation(input,fontName)
        else payyansService.ascii2Unicode(input, fontName)

    @PostMapping("/payyans/unicode2ascii/{fontName}")
    fun unicode2ascii(
        @RequestBody input: String,
        @PathVariable fontName: String,
        @RequestParam normalise: Boolean
    ) =
        if(normalise) payyansService.unicode2AsciiWithNormalisation(input, fontName)
        else payyansService.unicode2Ascii(input,fontName)

}
