package org.libindic.libindik.app.api

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@SpringBootTest
@AutoConfigureMockMvc
class ApiTest(@Autowired val mvc: MockMvc){
    @Test
    fun normaliseString() {
        mvc.perform(post("/normaliser")
            .contentType(MediaType.TEXT_PLAIN)
            .content("പൂമ്പാററ"))
            .andExpect(status().is2xxSuccessful)
            .andExpect(content().string("പൂമ്പാറ്റ"))
    }

    @Test
    fun asciiToUnicodeWithoutNormalisation() {
        mvc.perform(post("/payyans/ascii2unicode/ambili")
            .param("normalise","false")
            .contentType(MediaType.TEXT_PLAIN)
            .content("aebmfw."))
            .andExpect(status().is2xxSuccessful)
            .andExpect(content().string("മലയാളം."))
    }
    @Test
    fun asciiToUnicodeWithNormalisation() {
        mvc.perform(post("/payyans/ascii2unicode/ambili")
            .param("normalise","true")
            .contentType(MediaType.TEXT_PLAIN)
            .content("aebmfw"))
            .andExpect(status().is2xxSuccessful)
            .andExpect(content().string("മലയാളം"))
    }

    @Test
    fun unicode2asciiWithoutNormalisation() {
        mvc.perform(post("/payyans/unicode2ascii/ambili")
            .param("normalise","false")
            .contentType(MediaType.TEXT_PLAIN)
            .content("മലയാളം."))
            .andExpect(status().is2xxSuccessful)
            .andExpect(content().string("aebmfw."))
    }
    @Test
    fun unicode2asciiWithNormalisation() {
        mvc.perform(post("/payyans/unicode2ascii/ambili")
            .param("normalise","true")
            .contentType(MediaType.TEXT_PLAIN)
            .content("മലയാളം."))
            .andExpect(status().is2xxSuccessful)
            .andExpect(content().string("aebmfw"))
    }
}
