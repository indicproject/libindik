plugins {

    id("io.gitlab.arturbosch.detekt").version("1.22.0") apply false

    kotlin("jvm") version "1.7.22" apply false
    kotlin("plugin.spring") version "1.7.22" apply false
}
