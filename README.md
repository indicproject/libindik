# What is this ? 

- A Kotlin port of libindic.org and the libraries powering it.
- `app` is an API built with Spring Boot.
- `lib` is a pure kotlin port of the python libraries like payyans.
- `cli` is a CLI interface for the libraries, written in kotlin.

# What works ?

## API
- Payyans

## Library
- Payyans : See 

## CLI
- Payyans

# TODO

Library/API/CLI support for

- Chardetails
- N-Gram
- Hyphenate
- ApproxSearch
- Soundex
- Syllabilizer
- Similar Texts
- Transliteration
- Stemmer
- Katapayadi Numbers
- SpellCheck
- Fortune
- UCA Sort
- Shingling

# Developing locally

The project is a regular `gradle` project that can be imported into any IDE that supports `gradle`.

If you want to use VS Code or another editor, you can run gradle using the `gradlew` script.

## Things to remember.

### UTF-8 
Make sure that the JVM starts up with the parameter `-Dfile.encoding=UTF8`.

This can also be exported via an environment variable

```shell
export _JAVA_OPTIONS="-Dfile.encoding=UTF8"
```

### Compiling cli to produce a native binary using GraalVM

You'd need:

- GraalVM (this project targets JVM 17, so get the corresponding GraalVM)
- Make sure that the GraalVM setup has the binary called `native-image`

Run the gradle task `nativeCompile`:

```shell
./gradlew nativeCompile
```

This will produce a standalone native binary in the directory `cli/build/native/nativeCompile`.