package org.libindic.libindik.libs

/***
 * From https://github.com/libindic/payyans/blob/master/libindic/payyans/core.py
 *
 * The original logic is from
 *
 * 1. Santhosh Thottingal <santhosh.thottingal@gmail.com>
 * 2. Nishan Naseer <nishan.naseer@gmail.com>
 * 3. Manu S Madhav <manusmad@gmail.com>
 * 4. Rajeesh K Nambiar <rajeeshknambiar@gmail.com>
 *
 * പയ്യന്‍ ആളു തരികിടയാകുന്നു. ആസ്കി വേറൊരു തരികിടയും.
 * തരികിടയെ തരികിടകൊണ്ടു നേരിടുന്നതാണു് ബുദ്ധി.
 * അമേരിക്കാ-ഇറാഖ് യുദ്ധം താഴെപ്പറയും വിധമാകുന്നു.
 *
 */

class Payyans(private val ruleFilesPath: String = "rules/payyans", val normaliser: Normaliser) {
    /*
     * പയ്യന്റെ ക്ലാസ് ഉന്നതകുലമാകുന്നു. ച്ചാല്‍ ആഢ്യന്‍ തന്നെ.
     * ഏ ക്ലാസ് പയ്യന്‍...!
     *
     */
    var fontMaps: MutableMap<String, Map<String, String>> = mutableMapOf()

    fun unicode2Ascii(unicodeInput: String, fontName: String): String {
        // maintaining another in memory map will be more efficient, but at this point
        // this is enough. തല്‍ക്കാലം ഇത്രയും മതി.
        val fontMap = getFontMap(fontName).map { it }.associate { it.value to it.key }
        //convert input into a stack.
        val inputStack = ArrayDeque<Char>()
        for(ch in unicodeInput){
            inputStack.addLast(ch)
        }
        return processU2A(inputStack,fontMap,"")
    }
    fun unicode2AsciiWithNormalisation(unicodeInput: String, fontName: String): String {
        val unicodeText = normaliser.normaliseText(unicodeInput)
        return unicode2Ascii(unicodeText,fontName)
    }

    fun ascii2Unicode(asciiInput: String, fontName: String): String {
        val fontMap = getFontMap(fontName)

        if (fontMap.isEmpty()) throw NoSuchElementException("map with name $fontMap is empty or does not exist")

        //convert input into a stack.
        val inputStack = ArrayDeque<Char>()
        for(ch in asciiInput){
            inputStack.addLast(ch)
        }
        return processA2U(input = inputStack,fontMap = fontMap)
    }

    fun ascii2UnicodeWithNormalisation(asciiInput: String, fontName: String): String {
        val normalisedText = normaliser.normaliseText(asciiInput)
        return ascii2Unicode(normalisedText,fontName)
    }

    fun processA2U(input:ArrayDeque<Char>, fontMap:Map<String,String>):String{
        if(input.size == 0) return ""
        val (unicodeLetter, _,nextInput) =  takeNextLetter(input,fontMap,MAX_ASCII_MAP_KEY_LENGTH)
        val (unicodeText,nInput) = processAsciiLetter(unicodeLetter,nextInput,fontMap)
        return unicodeText.plus(processA2U(nInput,fontMap))
    }


    fun processU2A(input:ArrayDeque<Char>,
                   fontMap:Map<String,String>,
                   asciiText:String):String{
        if(input.size == 0) return asciiText
        val (asciiLetter, unicodeLetter,nextInput) =  takeNextLetter(input,fontMap,MAX_UNICODE_MAP_KEY_LENGTH)
        val newAsciiText = if (pseudoPostbases.containsValue(unicodeLetter)) {
            // മുമ്പിലൊന്നും പിറകിലൊന്നും
            asciiText.dropLast(1) + asciiLetter[0] +
                    asciiText.last() + asciiLetter[1]
        } else if (prebaseLetters.contains(unicodeLetter)) {
            // പിറകില്‍ രണ്ടു സാധനം പിടിപ്പിക്കുക
            asciiText.dropLast(1) + asciiLetter +
                    asciiText.last()
        } else {
            asciiText + asciiLetter
        }
        return processU2A(nextInput,fontMap,newAsciiText)
    }


    fun getFontMap(fontName: String): Map<String, String> {
        if (fontMaps.get(fontName).isNullOrEmpty()) {
            val fontMap = readMappingRulesFromFile("$ruleFilesPath/$fontName.map")!!
            fontMaps.put(fontName, fontMap)
        }
        return fontMaps.getOrDefault(fontName, mutableMapOf())
    }

}

// These really are standalone functions and variables.
const val MAX_ASCII_MAP_KEY_LENGTH = 2
const val MAX_UNICODE_MAP_KEY_LENGTH = 3
val prebaseLetters =  listOf<String>("േ", "ൈ", "ൊ", "ോ", "ൌ", "്ര", "െ")
val postbaseLetters =  listOf<String>("്യ", "്വ")
val signedVowels = listOf<String>("എ","ഒ")
val vowelSignMap = mapOf<String,String>(
    "എ"+"െ" to "ഐ",
    "ഒ"+"ാ" to "ഓ",
    "ഒ" + "ൗ" to "ഔ"
)
val pseudoPostbases = mapOf<String,String>(
    "െ" + "ാ" to "ൊ",
    "േ" + "ാ" to  "ോ",
    "െ" + "ൗ" to "ൌ"
)
/*
ഇതെന്തിനാന്നു ചോദിച്ചാ, ഈ അക്ഷരങ്ങളുടെ ഇടതു വശത്തെഴുതുന്ന
സ്വര ചിഹ്നങ്ങളുണ്ടല്ലോ? അവ ആസ്കി തരികിടയില്‍ എഴുതുന്നതു് ഇടതു വശത്തു
തന്നെയാ. യൂണിക്കോഡില്‍ അക്ഷരത്തിനു ശേഷവും. അപ്പൊ ആ വക സംഭവങ്ങളെ
തിരിച്ചറിയാനാണു് ഈ സംഭവം.
"തരികിട തരികിടോ ധീംതരികിട" (തരികിട തരികിടയാല്‍)
എന്നു പയ്യന്റെ ഗുരു പയ്യഗുരു പയ്യെ മൊഴിഞ്ഞിട്ടുണ്ടു്.
'*/
fun isPrebase(letter: String): Boolean {
    return (letter in prebaseLetters) // "ഇതു സത്യം... അ...സത്യം.... അസത്യം...!"
}

/*
"ക്യ" എന്നതിലെ "്യ", "ക്വ" എന്നതിലെ "്വ" എന്നിവ പോസ്റ്റ്-ബേസ് ആണ്.
"ത്യേ" എന്നത് ആസ്കിയില്‍ "ഏ+ത+്യ" എന്നാണ് എഴുതുന്നത്.
അപ്പോള്‍ വ്യഞ്ജനം കഴിഞ്ഞ് പോസ്റ്റ്-ബേസ് ഉണ്ടെങ്കില്‍
വ്യഞ്ജനം+പോസ്റ്റ്-ബേസ് കഴിഞ്ഞേ പ്രീ-ബേസ് ചേര്‍ക്കാവൂ!
("ഏ+ത+്യ" = എന്നതില്‍ ത ആണ് വ്യഞ്ജനം. "്യ" പോസ്റ്റ്ബേസും "ഏ" ("േ") പ്രീബേസും.
ഹൊ, പയ്യന്‍ പാണിനീശിഷ്യനാണ്!!
*/
fun isPostbase(letter: String): Boolean {
    return (letter in postbaseLetters)
}

fun getVowelSign(vowel: String, vowelSign: String): String {
    return vowelSignMap.getOrDefault( vowel + vowelSign, vowel+vowelSign)
}
fun processAsciiLetter(letter: String,
                       input:ArrayDeque<Char>,
                       fontMap:Map<String,String>):Pair<String,ArrayDeque<Char>>{
    //look ahead, get to the next letter
    val (nextLetter,_,input1) = takeNextLetter(input,fontMap,2)
    // check if the next one is postbase.
    val (nextPlusOneLetter,_,input2) = takeNextLetter(input1,fontMap,2)
    val (nextPlusTwoLetter,_,input3) = takeNextLetter(input2,fontMap,2)

    if(isPrebase(letter)) {
        return if (isPostbase(nextPlusOneLetter)) {
            Pair(nextLetter + nextPlusOneLetter + letter, input2)
        }// check for very specific case of
        // prebase1 + prebase2 + vyanjanam + prebase3 where prebase1 + prebase3 is a pseudo postbase
         else if( ( isPrebase(nextLetter)) && (!isPostbase(nextPlusOneLetter))
         && (!isPrebase(nextPlusOneLetter)) && (pseudoPostbases.containsKey(letter+nextPlusTwoLetter)) ){
             Pair(nextPlusOneLetter+nextLetter+pseudoPostbases[letter+nextPlusTwoLetter],input3)
        }
        else if( pseudoPostbases.containsKey(letter+nextPlusOneLetter)){
            Pair(nextLetter+ pseudoPostbases[letter+nextPlusOneLetter]!!, input2)
        }
        else if (nextLetter in signedVowels) {
            Pair(getVowelSign(letter, nextLetter),input1)
        }
        else{
            Pair(nextLetter+letter,input1)
        }
    }

    //nothing else to do. Just return the thing.
    return Pair(letter,input)
}


fun takeNextLetter(input:ArrayDeque<Char>,
                   fontMap: Map<String, String>,
                   maxLetter:Int):  Triple<String,String,ArrayDeque<Char>>{

    for(charNo in maxLetter downTo 1){
        val elements = input.take(charNo)
        val key = elements.joinToString("")
        if (key in fontMap.keys ){
            return Triple(fontMap[key]!!,key, ArrayDeque<Char>(input.drop(charNo)))
        }
    }
    val output = if(input.size == 0){
        ""
    }else{
        input.removeFirst()
    }

    return Triple(output.toString(),output.toString(),input)
}
