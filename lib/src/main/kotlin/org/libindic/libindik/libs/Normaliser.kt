package org.libindic.libindik.libs

import java.util.logging.Logger


class Normaliser(private val rulesFilePath: String = "rules/normaliser/rules.map") {
    companion object {
        val log = Logger.getLogger(this::class.java.canonicalName)
    }
    var ruleSet: Map<String, String> = emptyMap()

    fun normaliseText(input: String): String {
        if (ruleSet.isEmpty()) {
            log.info("loading rules from $rulesFilePath")
            ruleSet = readMappingRulesFromFile(rulesFilePath)!!
        }
        val words = input.split(" ")

        return words.joinToString(separator = " ") {
            val word = trim(it)
            val wordLength = word.length
            var suffixPosItr = 2
            var wordStemmed = ""
            while (suffixPosItr < wordLength) {
                val suffix = word.substring(suffixPosItr, wordLength)
                if (suffix in ruleSet) {
                    wordStemmed = word.substring(0, suffixPosItr) + ruleSet[suffix]
                    break
                }
                suffixPosItr += 1
            }
            if (wordStemmed == "") wordStemmed = word
            wordStemmed
        }.trim()
    }

    fun trim(input: String): String {
        val punctuations = listOf(
            '~', '!', '@', '#', '$', '%', '^', '&', '*',
            '(', ')', '-', '+', '_', '=', '{', '}', '|',
            ' ', ':', ';', '<', '>', ',', '.', '?',
        )
        val word = input.trim()

        val strippedChars = word.toCharArray().filter { !punctuations.contains(it) }.toCharArray()
        return String(strippedChars)
    }
}
