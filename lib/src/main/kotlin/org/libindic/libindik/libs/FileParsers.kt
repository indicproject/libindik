package org.libindic.libindik.libs

import java.io.FileNotFoundException
import java.nio.charset.StandardCharsets

fun readMappingRulesFromFile(rulesFilePath: String): Map<String, String> {
    val lines = object {}
        .javaClass.classLoader.getResourceAsStream(rulesFilePath)
        ?.bufferedReader(StandardCharsets.UTF_8)?.readLines()
        ?: throw FileNotFoundException("rules file at $rulesFilePath cannot be found.")

    return lines.map {
        it.trim()
    }.filter {
        it != "" && it.contains("=") && !it.startsWith("#")
    }.map {
        val tokens = it.split("=")
        if (tokens.size > 2) throw AssertionError("error parsing $it : too many equal signs in one line")
        tokens
    }.associate { it[0].trim() to it[1].trim() }
}
//
//fun readCharMapsFromDirectory(charMapsDirectory: String): Map<String, List<String>> {
//    val directory = object {}
//        .javaClass.classLoader.getResource(charMapsDirectory)
//        ?.file
//        ?: throw FileNotFoundException("directory $charMapsDirectory cannot be found.")
//
//    return lines.map {
//        it.trim()
//    }.filter {
//        it != "" && it.contains("=") && !it.startsWith("#")
//    }.map {
//        val tokens = it.split("=")
//        if (tokens.size > 2) throw AssertionError("error parsing $it : too many equal signs in one line")
//        tokens
//    }.associate { it[0].trim() to it[1].trim() }
//}
//
//fun readCharMapsFromFile(rulesFilePath: String): List<String> {
//    val lines = object {}
//        .javaClass.classLoader.getResourceAsStream(rulesFilePath)
//        ?.bufferedReader(StandardCharsets.UTF_8)?.readLines()
//        ?: throw FileNotFoundException("rules file at $rulesFilePath cannot be found.")
//
//    return lines.map {
//        it.trim()
//    }.filter {
//        it != "" && it.contains("=") && !it.startsWith("#")
//    }.map {
//        val tokens = it.split("=")
//        if (tokens.size > 2) throw AssertionError("error parsing $it : too many equal signs in one line")
//        tokens
//    }.associate { it[0].trim() to it[1].trim() }
//}
