package org.libindic.libindik.libs

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class PayyansTest {
    @Test
    fun `should normalise and convert given ascii text to unicode according to font map name`() {
        val normaliser = Normaliser("rules/normaliser/rules.map")
        val payyans = Payyans("rules/payyans", normaliser)
        assertEquals("നിത്യേനയുള്ളതിന് വിലയൊട്ടുമില്ല",
            payyans.ascii2UnicodeWithNormalisation("\\nXty\\bp≈Xnå hnesbm´pan√", "ambili"))
    }

    @Test
    fun `should convert പൊളി to unicode according to font map name`() {
        val normaliser = Normaliser("rules/normaliser/rules.map")
        val payyans = Payyans("rules/payyans", normaliser)
        assertEquals("പൊളി",
            payyans.ascii2Unicode("s]mfn", "karthika"))
    }

    @Test
    fun `should convert പൊളി to ascii according to font map name`() {
        val normaliser = Normaliser("rules/normaliser/rules.map")
        val payyans = Payyans("rules/payyans", normaliser)
        assertEquals("s]mfn",
            payyans.unicode2Ascii("പൊളി", "karthika"))
    }

    //s{]m^kÀ
    @Test
    fun `should convert പ്രൊഫസര്‍ to unicode according to font map name`() {
        val normaliser = Normaliser("rules/normaliser/rules.map")
        val payyans = Payyans("rules/payyans", normaliser)
        assertEquals("പ്രൊഫസര്‍",
            payyans.ascii2Unicode("s{]m^kÀ", "karthika"))
    }

    @Test
    fun `should normalise and convert given unicode text to ascii according to font map name`() {
        val normaliser = Normaliser("rules/normaliser/rules.map")
        val payyans = Payyans("rules/payyans", normaliser)
        assertEquals("\\nXty\\bp≈Xnå hnesbm´pan√",
            payyans.unicode2AsciiWithNormalisation("നിത്യേനയുള്ളതിന് വിലയൊട്ടുമില്ല", "ambili"))
    }



    @Test
    fun `should convert given ascii text to unicode according to font map name`(){
        val normaliser = Normaliser("rules/normaliser/rules.map")
        val payyans = Payyans("rules/payyans", normaliser)
        assertEquals("മലയാളം.", payyans.ascii2Unicode("aebmfw.", "ambili"))
    }

    @Test
    fun `should convert given unicode text to ascii according to font map name`() {
        val normaliser = Normaliser("rules/normaliser/rules.map")
        val payyans = Payyans("rules/payyans", normaliser)
        assertEquals("aebmfw.", payyans.unicode2Ascii("മലയാളം.", "ambili"))
    }
}
