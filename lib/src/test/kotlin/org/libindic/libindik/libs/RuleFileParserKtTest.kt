package org.libindic.libindik.libs

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class RuleFileParserKtTest {
    @Test
    fun `should parse a valid map or rules file`() {
        val actual = readMappingRulesFromFile("rules/test.map")
        val expected = mapOf<String, String>(
            "!" to "!",
            "\"" to "-",
            "(" to "(",
            ")" to ")",
            "*" to "*",
            "+" to "+",
        )
        assertEquals(expected, actual)
    }
}
