package org.libindic.libindik.libs

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class NormaliserTest {

    @Test
    fun `given a word punctuations should be trimmed`() {
        val normaliser = Normaliser("")
        assertEquals("ആകാശം", normaliser.trim(", ആകാശം.+"))
    }

    @Test
    fun `given sentence should be normalised`() {
        val normaliser = Normaliser("rules/normaliser/rules.map")
        assertEquals("ആകാശം", normaliser.normaliseText(", ആകാശം.+"))
        assertEquals("പൂമ്പാറ്റ", normaliser.normaliseText("പൂമ്പാററ"))
        // ൺൻർൽൾൿ are atomic chillus and should get
        // converted to ണ്‍ന്‍ര്‍ല്‍ള്‍ക്‍ respectively
        assertEquals("അവില്‍", normaliser.normaliseText("അവിൽ"))
        assertEquals("രമണന്‍", normaliser.normaliseText("രമണൻ"))
        assertEquals("അവള്‍", normaliser.normaliseText("അവൾ"))
        assertEquals("ശ്രാവണ്‍", normaliser.normaliseText("ശ്രാവൺ"))
    }
}
