import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {

    id("io.gitlab.arturbosch.detekt").version("1.22.0")
    kotlin("jvm") version "1.7.22"
    kotlin("plugin.spring") version "1.7.22"
}

group = "org.libindic.libindik"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.2"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}
repositories {
    mavenCentral()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

// tasks["test"].dependsOn("copyRules")
tasks.withType<Test> {
    systemProperty("file.encoding", "utf-8")
    useJUnitPlatform()
}

detekt {
    // Define the detekt configuration(s) you want to use.
    // Defaults to the default detekt configuration.
    config = files("config/detekt/detekt.yml")
}
