import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.22"
    application
    // Apply GraalVM Native Image plugin
    id("org.graalvm.buildtools.native") version "0.9.20"
}

group = "org.libindic.libindik"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":libindik"))

    implementation("com.github.ajalt.clikt:clikt:3.5.0")

}

graalvmNative {
    binaries {
        named("main") {
            javaLauncher.set(javaToolchains.launcherFor {
                languageVersion.set(JavaLanguageVersion.of(17))
                vendor.set(JvmVendorSpec.matching("GraalVM Community"))
            })
        }
    }
}

application {
    mainClass.set("org.libindic.libindik.cli.ApplicationKt")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

// tasks["test"].dependsOn("copyRules")
tasks.withType<Test> {
    systemProperty("file.encoding", "utf-8")
    useJUnitPlatform()
}
