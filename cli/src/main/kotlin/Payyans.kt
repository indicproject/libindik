package org.libindic.libindik.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.file
import org.libindic.libindik.libs.Normaliser
import java.io.FileNotFoundException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

class Payyans: CliktCommand(help="Payyans Ascii <-> Unicode conversion utility") {
    val input by option("-i", "--input",
        help = "File containing ASCII/Unicode text to be converted.")
        .file(mustExist = true,mustBeReadable=true)
    val output by option("-o", "--output",
        help = "File to which the converted content should be written.")
        .file(mustExist = false)
    val fontMap by option("-f", "--font",
        help = "Name of font map to use for conversion.")
    val normaliseFlag by option("--normalise", "-n",
        help = "If this flag is present, normalisation would be applied.")
        .flag(default = false)
    val direction by option("-d", "--direction",
        help = "Determines whether ascii is converted to unicode or vice versa")
        .choice("a2u", "u2a").default("a2u")
    override fun run() {
        //run a round of checks
        if(input == null){
            echo("An input file should be provided.")
            return
        }
        if (output == null) {
            echo("An output file should be provided.")
            return
        }
        if (fontMap == null) {
            echo("A fontmap should be specified.")
            return
        }
        //ready the payyans.
        val normaliser = Normaliser()
        val payyans = org.libindic.libindik.libs.Payyans(normaliser=normaliser)

        val lines = input
            ?.bufferedReader(StandardCharsets.UTF_8)?.readLines()
            ?: throw FileNotFoundException("input file cannot be read.")

        val outputContent = if(direction.equals("a2u")) lines.map{
            if(normaliseFlag) payyans.ascii2UnicodeWithNormalisation(it,fontMap!!)
            else payyans.ascii2Unicode(it,fontMap!!)
        }else lines.map{
            if(normaliseFlag)  payyans.unicode2AsciiWithNormalisation(it,fontMap!!)
            else payyans.unicode2Ascii(it,fontMap!!)
        }
        Files.writeString(Paths.get(output?.path),outputContent.joinToString("\n"))
        echo("finished converting ${input!!.path} to ${output?.path}")
    }
}