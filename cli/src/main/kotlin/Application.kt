package org.libindic.libindik.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.prompt
import com.github.ajalt.clikt.parameters.types.file
import com.github.ajalt.clikt.parameters.types.path
import org.libindic.libindik.libs.Normaliser
import java.io.FileNotFoundException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths

class IndikCLI: CliktCommand(name="indikcli") {
    override fun run() = Unit
}

fun main(args: Array<String>) = IndikCLI()
    .subcommands(Payyans())
    .main(args)