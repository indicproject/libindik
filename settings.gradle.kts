rootProject.name = "libindik"
include("app")
include("lib")
include("cli")
project(":lib").name = "libindik"
project(":cli").name = "indikcli"
project(":app").name = "indik-web"
